package pl.marwit44.orderNowbackend.exceptions;

public class ItemNotFoundException extends Throwable {
    public ItemNotFoundException(String message) {
        super(message);
    }
}
