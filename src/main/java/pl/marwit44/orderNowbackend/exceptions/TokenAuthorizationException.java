package pl.marwit44.orderNowbackend.exceptions;

public class TokenAuthorizationException extends Throwable {
    public TokenAuthorizationException(String message) {
        super(message);
    }
}
