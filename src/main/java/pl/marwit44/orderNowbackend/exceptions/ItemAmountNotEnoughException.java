package pl.marwit44.orderNowbackend.exceptions;

public class ItemAmountNotEnoughException extends Throwable {
    public ItemAmountNotEnoughException(String message) {
        super(message);
    }
}
