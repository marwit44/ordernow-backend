package pl.marwit44.orderNowbackend.exceptions;

public class ItemDetailsNotFoundException extends Throwable {
    public ItemDetailsNotFoundException(String message) {
        super(message);
    }
}
