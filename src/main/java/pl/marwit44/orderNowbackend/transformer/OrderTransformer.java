package pl.marwit44.orderNowbackend.transformer;

import pl.marwit44.orderNowbackend.dto.Order;
import pl.marwit44.orderNowbackend.model.OrderModel;

import java.util.stream.Collectors;

public class OrderTransformer {

    static Order getOrderDtoFromModel(OrderModel orderModel) {
        Order order = new Order();
        order.setId(orderModel.getId());
        order.setPrice(orderModel.getPrice());
        order.setUserId(orderModel.getUserId());
        order.setStatus(orderModel.getStatus());
        order.setAccepted(orderModel.isAccepted());
        order.setAddDate(orderModel.getAddDate());
        return order;
    }

    static OrderModel getOrderModelFromDto(Order order, OrderModel orderModel) {
        orderModel.setId(order.getId());
        orderModel.setPrice(order.getPrice());
        orderModel.setUserId(order.getUserId());
        orderModel.setStatus(order.getStatus());
        orderModel.setAccepted(order.isAccepted());
        return orderModel;
    }

    public static Order getOrderDtoFromModelWithItemList(OrderModel orderModel) {

        Order order = getOrderDtoFromModel(orderModel);

        if (orderModel.getItemList() != null) {
            order.setItemList(orderModel.getItemList().stream().map(ItemInOrderTransformer::getItemInOrderDtoFromModel).collect(Collectors.toList()));
        }

        return order;
    }

    public static OrderModel getOrderModelFromDtoWithItemList(Order order, OrderModel orderModel) {

        final OrderModel orderModelFromDto = getOrderModelFromDto(order, orderModel);

        if(order.getItemList() != null) {
            orderModel.setItemList(order.getItemList().stream().map(e -> ItemInOrderTransformer.getItemInOrderModelFromDto(e,orderModelFromDto)).collect(Collectors.toList()));
        }

        return orderModel;
    }
}
