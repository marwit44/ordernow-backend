package pl.marwit44.orderNowbackend.transformer;

import pl.marwit44.orderNowbackend.dto.Item;
import pl.marwit44.orderNowbackend.dto.ItemInOrder;
import pl.marwit44.orderNowbackend.model.ItemInOrderModel;
import pl.marwit44.orderNowbackend.model.OrderModel;

import java.time.LocalDateTime;

public class ItemInOrderTransformer {

    static ItemInOrderModel getItemInOrderModelFromDto(ItemInOrder itemInOrder, OrderModel orderModel) {
        ItemInOrderModel itemInOrderModel = new ItemInOrderModel();
        itemInOrderModel.setId(itemInOrder.getId());
        itemInOrderModel.setName(itemInOrder.getName());
        itemInOrderModel.setPrice(itemInOrder.getPrice());
        itemInOrderModel.setAmount(itemInOrder.getAmount());
        itemInOrderModel.setOrder(OrderTransformer.getOrderModelFromDto(itemInOrder.getOrder(),orderModel));
        return itemInOrderModel;
    }

    public static ItemInOrderModel getItemInOrderModelFromItemDto(Item item) {
        ItemInOrderModel itemInOrderModel = new ItemInOrderModel();
        itemInOrderModel.setName(item.getName());
        itemInOrderModel.setPrice(item.getPrice());
        itemInOrderModel.setAmount(item.getAmount());
        if(item.getAddDate() == null) {
            itemInOrderModel.setAddDate(LocalDateTime.now());
        }
        return itemInOrderModel;
    }

    public static ItemInOrder getItemInOrderDtoFromModel(ItemInOrderModel itemInOrderModel) {
        ItemInOrder itemInOrder = new ItemInOrder();
        itemInOrder.setId(itemInOrderModel.getId());
        itemInOrder.setAmount(itemInOrderModel.getAmount());
        itemInOrder.setName(itemInOrderModel.getName());
        itemInOrder.setPrice(itemInOrderModel.getPrice());
        itemInOrder.setOrder(OrderTransformer.getOrderDtoFromModel(itemInOrderModel.getOrder()));
        return itemInOrder;
    }
}
