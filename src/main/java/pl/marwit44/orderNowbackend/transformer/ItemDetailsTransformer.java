package pl.marwit44.orderNowbackend.transformer;


import pl.marwit44.orderNowbackend.dto.ItemDetails;
import pl.marwit44.orderNowbackend.model.ImageModel;
import pl.marwit44.orderNowbackend.model.ItemDetailsModel;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

public class ItemDetailsTransformer {

    public static ItemDetails getItemDetailsDtoFromModel(ItemDetailsModel itemDetailsModel) {

        ItemDetails itemDetails = new ItemDetails();
        itemDetails.setId(itemDetailsModel.getId());
        itemDetails.setDescription(itemDetailsModel.getDescription());
        if (itemDetailsModel.getItemModel() != null) {
            itemDetails.setItem(ItemTransformer.getItemDtoFromModel(itemDetailsModel.getItemModel()));
        }
        itemDetails.setAddDate(itemDetailsModel.getAddDate());

        return itemDetails;
    }

    public static ItemDetailsModel getItemDetailsModelFromDto(ItemDetails itemDetails, ItemDetailsModel itemDetailsModel) {
        itemDetailsModel.setId(itemDetails.getId());
        itemDetailsModel.setDescription(itemDetails.getDescription());
        if (itemDetails.getItem() != null && itemDetailsModel.getItemModel() != null) {
            itemDetailsModel.setItemModel(ItemTransformer.getItemModelFromDto(itemDetails.getItem(), itemDetailsModel.getItemModel()));
        }

        if (itemDetails.getAddDate() == null && itemDetailsModel.getAddDate() == null) {
            itemDetailsModel.setAddDate(LocalDateTime.now());
        }
        return itemDetailsModel;
    }

    public static ItemDetails getItemDetailsDtoFromModelWithImageList(ItemDetailsModel itemDetailsModel) {

        ItemDetails itemDetails = getItemDetailsDtoFromModel(itemDetailsModel);

        if (itemDetailsModel.getImageList() != null) {
            itemDetails.setImageList(itemDetailsModel.getImageList().stream().map(ImageTransformer::getImageDtoFromModel).collect(Collectors.toList()));
        }

        return itemDetails;
    }

    public static ItemDetailsModel getItemDetailsModelFromDtoWithItemList(ItemDetails itemDetails, ItemDetailsModel itemDetailsModel) {

        getItemDetailsModelFromDto(itemDetails, itemDetailsModel);

        if (itemDetails.getImageList() != null) {
            itemDetailsModel.setImageList(itemDetails.getImageList().stream().map(e -> ImageTransformer.getImageModelFromDto(e, new ImageModel())).collect(Collectors.toList()));
        }

        return itemDetailsModel;
    }
}