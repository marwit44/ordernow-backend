package pl.marwit44.orderNowbackend.transformer;

import pl.marwit44.orderNowbackend.dto.Image;
import pl.marwit44.orderNowbackend.model.ImageModel;
import pl.marwit44.orderNowbackend.model.ItemDetailsModel;

import java.time.LocalDateTime;

public class ImageTransformer {

    public static Image getImageDtoFromModel(ImageModel imageModel) {

        Image image = new Image();
        image.setId(imageModel.getId());
        image.setName(imageModel.getName());
        image.setType(imageModel.getType());
        image.setPicture(imageModel.getPicture());
        image.setAddDate(imageModel.getAddDate());

        if (imageModel.getItemDetailsModel() != null) {
            image.setItemDetails(ItemDetailsTransformer.getItemDetailsDtoFromModel(imageModel.getItemDetailsModel()));
        }

        return image;
    }

    public static ImageModel getImageModelFromDto(Image image, ImageModel imageModel) {

        imageModel.setId(image.getId());
        imageModel.setName(image.getName());
        imageModel.setType(image.getType());
        imageModel.setPicture(image.getPicture());

        if (image.getItemDetails() != null) {
            imageModel.setItemDetailsModel(ItemDetailsTransformer.getItemDetailsModelFromDto(image.getItemDetails(), new ItemDetailsModel()));
        }

        if (imageModel.getAddDate() == null) {
            imageModel.setAddDate(LocalDateTime.now());
        }

        return imageModel;
    }
}
