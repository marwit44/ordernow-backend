package pl.marwit44.orderNowbackend.transformer;

import pl.marwit44.orderNowbackend.dto.Item;
import pl.marwit44.orderNowbackend.dto.ItemInOrder;
import pl.marwit44.orderNowbackend.model.ItemModel;

import java.time.LocalDateTime;

public class ItemTransformer {

    public static Item getItemDtoFromModel(ItemModel itemModel) {
        Item item = new Item();
        item.setId(itemModel.getId());
        item.setAmount(itemModel.getAmount());
        item.setPrice(itemModel.getPrice());
        item.setName(itemModel.getName());
        return item;
    }

    public static ItemModel getItemModelFromDto(Item item, ItemModel itemModel) {
        itemModel.setId(item.getId());
        itemModel.setName(item.getName());
        itemModel.setPrice(item.getPrice());
        itemModel.setAmount(item.getAmount());
        if(item.getAddDate() == null && itemModel.getAddDate() == null) {
            itemModel.setAddDate(LocalDateTime.now());
        }
        return itemModel;
    }

    public static ItemModel getItemModelFromItemInOrder(ItemInOrder item, ItemModel itemModel) {
        itemModel.setName(item.getName());
        itemModel.setPrice(item.getPrice());
        itemModel.setAmount(item.getAmount());
        return itemModel;
    }
}
