package pl.marwit44.orderNowbackend.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "Item")
public class ItemModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_seq_generator")
    @SequenceGenerator(name = "item_seq_generator", sequenceName = "item_id_seq", allocationSize = 1)
    private Long id;

    @Column(unique=true)
    private String name;

    private double price;

    private int amount;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "itemModel", cascade = CascadeType.ALL)
    private ItemDetailsModel itemDetailsModel;

    @CreationTimestamp
    @Column(name = "add_date", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime addDate;

    @UpdateTimestamp
    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Version
    private Long version = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public ItemDetailsModel getItemDetailsModel() {
        return itemDetailsModel;
    }

    public void setItemDetailsModel(ItemDetailsModel itemDetailsModel) {
        this.itemDetailsModel = itemDetailsModel;
    }

    public LocalDateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(LocalDateTime creationDate) {
        this.addDate = creationDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public static final class Builder {
        private String name;
        private double price;
        private int amount;
        private LocalDateTime addDate;

        private Builder() {
        }

        public static Builder anItemModel() {
            return new Builder();
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withPrice(double price) {
            this.price = price;
            return this;
        }

        public Builder withAmount(int amount) {
            this.amount = amount;
            return this;
        }

        public Builder withAddDate(LocalDateTime addDate) {
            this.addDate = addDate;
            return this;
        }

        public ItemModel build() {
            ItemModel itemModel = new ItemModel();
            itemModel.setName(name);
            itemModel.setPrice(price);
            itemModel.setAmount(amount);
            itemModel.setAddDate(addDate);
            Long version = 0L;
            itemModel.setVersion(version);
            return itemModel;
        }
    }
}
