package pl.marwit44.orderNowbackend.model;

public enum UserRole {
    ROLE_USER,
    ROLE_ADMIN
}
