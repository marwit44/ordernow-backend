package pl.marwit44.orderNowbackend.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "item_details")
public class ItemDetailsModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "item_details_seq_generator")
    @SequenceGenerator(name = "item_details_seq_generator", sequenceName = "item_details_id_seq", allocationSize = 1)
    private Long id;

    private String description;

    @OneToOne(fetch = FetchType.LAZY)
    @PrimaryKeyJoinColumn
    private ItemModel itemModel;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "itemDetailsModel", cascade = CascadeType.ALL)
    private List<ImageModel> imageList;

    @CreationTimestamp
    @Column(name = "add_date", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime addDate;

    @UpdateTimestamp
    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Version
    private Long version = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ItemModel getItemModel() {
        return itemModel;
    }

    public void setItemModel(ItemModel itemModel) {
        this.itemModel = itemModel;
    }

    public List<ImageModel> getImageList() {
        return imageList;
    }

    public void setImageList(List<ImageModel> imageList) {
        this.imageList = imageList;
    }

    public LocalDateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(LocalDateTime addDate) {
        this.addDate = addDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public static final class Builder {
        private Long id;
        private String description;
        private ItemModel itemModel;
        private List<ImageModel> imageList;
        private LocalDateTime addDate;
        private LocalDateTime updateDate;
        private Long version = 0L;

        private Builder() {
        }

        public static Builder anItemDetailsModel() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withItemModel(ItemModel itemModel) {
            this.itemModel = itemModel;
            return this;
        }

        public Builder withImageList(List<ImageModel> imageList) {
            this.imageList = imageList;
            return this;
        }

        public Builder withAddDate(LocalDateTime addDate) {
            this.addDate = addDate;
            return this;
        }

        public Builder withUpdateDate(LocalDateTime updateDate) {
            this.updateDate = updateDate;
            return this;
        }

        public Builder withVersion(Long version) {
            this.version = version;
            return this;
        }

        public ItemDetailsModel build() {
            ItemDetailsModel itemDetailsModel = new ItemDetailsModel();
            itemDetailsModel.setId(id);
            itemDetailsModel.setDescription(description);
            itemDetailsModel.setItemModel(itemModel);
            itemDetailsModel.setImageList(imageList);
            itemDetailsModel.setAddDate(addDate);
            itemDetailsModel.setUpdateDate(updateDate);
            itemDetailsModel.setVersion(version);
            return itemDetailsModel;
        }
    }
}
