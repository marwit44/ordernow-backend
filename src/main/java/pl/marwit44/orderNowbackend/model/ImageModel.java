package pl.marwit44.orderNowbackend.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "image_table")
public class ImageModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "image_seq_generator")
    @SequenceGenerator(name = "image_seq_generator", sequenceName = "image_id_seq", allocationSize = 1)
    private Long id;

    private String name;

    private String type;

    @Column(length = 1000)
    private byte[] picture;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "item_details_id", nullable = false)
    private ItemDetailsModel itemDetailsModel;

    @CreationTimestamp
    @Column(name = "add_date", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime addDate;

    @UpdateTimestamp
    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Version
    private Long version = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public ItemDetailsModel getItemDetailsModel() {
        return itemDetailsModel;
    }

    public void setItemDetailsModel(ItemDetailsModel itemDetailsModel) {
        this.itemDetailsModel = itemDetailsModel;
    }

    public LocalDateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(LocalDateTime addDate) {
        this.addDate = addDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public static final class Builder {
        private Long id;
        private String name;
        private String type;
        private byte[] picByte;
        private ItemDetailsModel itemDetailsModel;
        private LocalDateTime addDate;
        private LocalDateTime updateDate;
        private Long version = 0L;

        private Builder() {
        }

        public static Builder anImageModel() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Builder withPicByte(byte[] picByte) {
            this.picByte = picByte;
            return this;
        }

        public Builder withItemDetailsModel(ItemDetailsModel itemDetailsModel) {
            this.itemDetailsModel = itemDetailsModel;
            return this;
        }

        public Builder withAddDate(LocalDateTime addDate) {
            this.addDate = addDate;
            return this;
        }

        public Builder withUpdateDate(LocalDateTime updateDate) {
            this.updateDate = updateDate;
            return this;
        }

        public Builder withVersion(Long version) {
            this.version = version;
            return this;
        }

        public ImageModel build() {
            ImageModel imageModel = new ImageModel();
            imageModel.setId(id);
            imageModel.setName(name);
            imageModel.setType(type);
            imageModel.setPicture(picByte);
            imageModel.setItemDetailsModel(itemDetailsModel);
            imageModel.setAddDate(addDate);
            imageModel.setUpdateDate(updateDate);
            imageModel.setVersion(version);
            return imageModel;
        }
    }
}
