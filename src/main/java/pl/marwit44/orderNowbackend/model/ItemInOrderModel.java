package pl.marwit44.orderNowbackend.model;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Table(name = "Item_in_order")
public class ItemInOrderModel implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "itemInOrder_seq_generator")
    @SequenceGenerator(name = "itemInOrder_seq_generator", sequenceName = "itemInOrder_id_seq", allocationSize = 1)
    private Long id;

    private String name;

    private double price;

    private int amount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ORDER_ID", nullable = false)
    private OrderModel order;

    @CreationTimestamp
    @Column(name = "add_date", nullable = false, updatable = false, columnDefinition = "TIMESTAMP")
    private LocalDateTime addDate;

    @UpdateTimestamp
    @Column(name = "update_date")
    private LocalDateTime updateDate;

    @Version
    private Long version = 0L;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public OrderModel getOrder() {
        return order;
    }

    public void setOrder(OrderModel order) {
        this.order = order;
    }

    public LocalDateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(LocalDateTime creationDate) {
        this.addDate = creationDate;
    }

    public LocalDateTime getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(LocalDateTime updateDate) {
        this.updateDate = updateDate;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public static final class Builder {
        private String name;
        private double price;
        private int amount;
        private OrderModel order;
        private LocalDateTime addDate;

        private Builder() {
        }

        public static Builder anItemInOrderModel() {
            return new Builder();
        }


        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withPrice(double price) {
            this.price = price;
            return this;
        }

        public Builder withAmount(int amount) {
            this.amount = amount;
            return this;
        }

        public Builder withOrder(OrderModel order) {
            this.order = order;
            return this;
        }

        public Builder withAddDate(LocalDateTime addDate) {
            this.addDate = addDate;
            return this;
        }

        public ItemInOrderModel build() {
            ItemInOrderModel itemInOrderModel = new ItemInOrderModel();
            itemInOrderModel.setName(name);
            itemInOrderModel.setPrice(price);
            itemInOrderModel.setAmount(amount);
            itemInOrderModel.setOrder(order);
            itemInOrderModel.setAddDate(addDate);
            Long version = 0L;
            itemInOrderModel.setVersion(version);
            return itemInOrderModel;
        }
    }
}
