package pl.marwit44.orderNowbackend.model;

public enum OrderStatus {

    PREPARATION,
    SENDING,
    READY_TO_DELIVER,
    DONE
}
