package pl.marwit44.orderNowbackend.dto;

import pl.marwit44.orderNowbackend.model.UserRole;

import java.time.LocalDateTime;

public class Token {

    private Long id;

    private LocalDateTime dateTime;

    private String value;

    private UserRole role;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public static final class Builder {
        private Long id;
        private LocalDateTime dateTime;
        private String value;
        private UserRole role;

        private Builder() {
        }

        public static Builder aToken() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withDateTime(LocalDateTime dateTime) {
            this.dateTime = dateTime;
            return this;
        }

        public Builder withValue(String value) {
            this.value = value;
            return this;
        }

        public Builder withRole(UserRole role) {
            this.role = role;
            return this;
        }

        public Token build() {
            Token token = new Token();
            token.setId(id);
            token.setDateTime(dateTime);
            token.setValue(value);
            token.setRole(role);
            return token;
        }
    }
}
