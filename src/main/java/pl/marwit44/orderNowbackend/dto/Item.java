package pl.marwit44.orderNowbackend.dto;

import java.time.LocalDateTime;

public class Item {

    private Long id;

    private int amount;

    private String name;

    private double price;

    private Token token;

    private LocalDateTime addDate;

    private int totalPages;

    public Item() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public LocalDateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(LocalDateTime addDate) {
        this.addDate = addDate;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }

    public static final class Builder {
        private int amount;
        private String name;
        private double price;

        private Builder() {
        }

        public static Builder anItem() {
            return new Builder();
        }

        public Builder withAmount(int amount) {
            this.amount = amount;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withPrice(double price) {
            this.price = price;
            return this;
        }

        public Item build() {
            Item item = new Item();
            item.setAmount(amount);
            item.setName(name);
            item.setPrice(price);
            return item;
        }
    }
}
