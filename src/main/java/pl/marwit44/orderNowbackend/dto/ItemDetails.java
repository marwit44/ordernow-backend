package pl.marwit44.orderNowbackend.dto;

import java.time.LocalDateTime;
import java.util.List;

public class ItemDetails {

    private Long id;

    private String description;

    private Item item;

    private List<Image> imageList;

    private LocalDateTime addDate;

    private Token token;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public List<Image> getImageList() {
        return imageList;
    }

    public void setImageList(List<Image> imageList) {
        this.imageList = imageList;
    }

    public LocalDateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(LocalDateTime addDate) {
        this.addDate = addDate;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public static final class Builder {
        private Long id;
        private String description;
        private Item item;
        private List<Image> imageList;
        private LocalDateTime addDate;
        private Token token;

        private Builder() {
        }

        public static Builder anItemDetails() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withDescription(String description) {
            this.description = description;
            return this;
        }

        public Builder withItem(Item item) {
            this.item = item;
            return this;
        }

        public Builder withImageList(List<Image> imageList) {
            this.imageList = imageList;
            return this;
        }

        public Builder withAddDate(LocalDateTime addDate) {
            this.addDate = addDate;
            return this;
        }

        public Builder withToken(Token token) {
            this.token = token;
            return this;
        }

        public ItemDetails build() {
            ItemDetails itemDetails = new ItemDetails();
            itemDetails.setId(id);
            itemDetails.setDescription(description);
            itemDetails.setItem(item);
            itemDetails.setImageList(imageList);
            itemDetails.setAddDate(addDate);
            itemDetails.setToken(token);
            return itemDetails;
        }
    }
}
