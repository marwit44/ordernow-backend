package pl.marwit44.orderNowbackend.dto;

import java.time.LocalDateTime;

public class Image {

    private Long id;

    private String name;

    private String type;

    private byte[] picture;

    private ItemDetails itemDetails;

    private LocalDateTime addDate;

    private Token token;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public ItemDetails getItemDetails() {
        return itemDetails;
    }

    public void setItemDetails(ItemDetails itemDetails) {
        this.itemDetails = itemDetails;
    }

    public LocalDateTime getAddDate() {
        return addDate;
    }

    public void setAddDate(LocalDateTime addDate) {
        this.addDate = addDate;
    }

    public Token getToken() {
        return token;
    }

    public void setToken(Token token) {
        this.token = token;
    }

    public static final class Builder {
        private Long id;
        private String name;
        private String type;
        private byte[] picture;
        private ItemDetails itemDetails;
        private LocalDateTime addDate;
        private Token token;

        private Builder() {
        }

        public static Builder anImage() {
            return new Builder();
        }

        public Builder withId(Long id) {
            this.id = id;
            return this;
        }

        public Builder withName(String name) {
            this.name = name;
            return this;
        }

        public Builder withType(String type) {
            this.type = type;
            return this;
        }

        public Builder withPicture(byte[] picture) {
            this.picture = picture;
            return this;
        }

        public Builder withItemDetails(ItemDetails itemDetails) {
            this.itemDetails = itemDetails;
            return this;
        }

        public Builder withAddDate(LocalDateTime addDate) {
            this.addDate = addDate;
            return this;
        }

        public Builder withToken(Token token) {
            this.token = token;
            return this;
        }

        public Image build() {
            Image image = new Image();
            image.setId(id);
            image.setName(name);
            image.setType(type);
            image.setPicture(picture);
            image.setItemDetails(itemDetails);
            image.setAddDate(addDate);
            image.setToken(token);
            return image;
        }
    }
}
