package pl.marwit44.orderNowbackend.specifications;

import org.springframework.data.jpa.domain.Specification;
import pl.marwit44.orderNowbackend.model.OrderModel;
import pl.marwit44.orderNowbackend.model.UserRole;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.time.LocalDateTime;

public class OrderSpecification implements Specification<OrderModel> {

    private final String searchTerm;
    private final Long id;
    private final Boolean accepted;
    private final UserRole role;
    private final String sortBy;
    private final Long rangeFrom;
    private final Long rangeTo;
    private final LocalDateTime rangeFromDateTime;
    private final LocalDateTime rangeToDateTime;

    public OrderSpecification(String searchTerm, Long id, Boolean accepted, UserRole role, String sortBy, Long rangeFrom, Long rangeTo, LocalDateTime rangeFromDateTime, LocalDateTime rangeToDateTime) {
        this.searchTerm = searchTerm;
        this.id = id;
        this.accepted = accepted;
        this.role = role;
        this.sortBy = sortBy;
        this.rangeFrom = rangeFrom;
        this.rangeTo = rangeTo;
        this.rangeFromDateTime = rangeFromDateTime;
        this.rangeToDateTime = rangeToDateTime;
    }

    private String containsPattern(String searchTerm) {
        return "%" + searchTerm.toLowerCase() + "%";
    }

    private Predicate getPredForSortByWithRange(Root<OrderModel> root, CriteriaBuilder criteriaBuilder) {

        Predicate predGreaterEqPrice = criteriaBuilder.greaterThanOrEqualTo(root.get("price"), rangeFrom);
        Predicate predLessEqPrice = criteriaBuilder.lessThanOrEqualTo(root.get("price"), rangeTo);

        Predicate predGreaterEqAddDate = criteriaBuilder.greaterThanOrEqualTo(root.get("addDate"), rangeFromDateTime);
        Predicate predLessEqAddDate = criteriaBuilder.lessThanOrEqualTo(root.get("addDate"), rangeToDateTime);

        if (rangeFrom > -1 && rangeTo > -1 && !sortBy.equals("status")) {
            if (sortBy.equals("price")) {
                return criteriaBuilder.and(
                        predGreaterEqPrice,
                        predLessEqPrice
                );
            } else {
                return criteriaBuilder.and(
                        predGreaterEqAddDate,
                        predLessEqAddDate);
            }
        } else if (rangeFrom > -1 && !sortBy.equals("status")) {
            if (sortBy.equals("price")) {
                return predGreaterEqPrice;
            } else {
                return predGreaterEqAddDate;
            }
        } else if (rangeTo > -1 && !sortBy.equals("status")) {
            if (sortBy.equals("price")) {
                return predLessEqPrice;
            } else {
                return predLessEqAddDate;
            }
        } else {
            return criteriaBuilder.and();
        }
    }

    @Override
    public Predicate toPredicate(Root<OrderModel> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {

        Predicate predUserIdAccepted = criteriaBuilder.and(
                criteriaBuilder.equal(root.get("userId").as(Long.class), id
                ),
                criteriaBuilder.equal(root.get("accepted").as(Boolean.class), accepted
                )
        );

        if (searchTerm == null) {
            if (role == UserRole.ROLE_USER) {
                return criteriaBuilder.and(getPredForSortByWithRange(root, criteriaBuilder),
                        predUserIdAccepted);
            } else {
                return criteriaBuilder.and(getPredForSortByWithRange(root, criteriaBuilder),
                        criteriaBuilder.equal(root.get("accepted").as(Boolean.class), accepted));
            }
        }

        Predicate predSearchTerm = criteriaBuilder.or(
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("price").as(String.class)), containsPattern(searchTerm)
                ),
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("addDate").as(String.class)), containsPattern(searchTerm)
                ),
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("status").as(String.class)), containsPattern(searchTerm)
                )
        );

        if (role == UserRole.ROLE_USER) {

            if (!sortBy.equals("status")) {
                return criteriaBuilder.and(
                        criteriaBuilder.and(
                                predSearchTerm,
                                getPredForSortByWithRange(root, criteriaBuilder)
                        ),
                        predUserIdAccepted
                );
            } else {
                return criteriaBuilder.and(
                        predSearchTerm,
                        predUserIdAccepted
                );
            }
        } else {
            if (!sortBy.equals("status")) {
                return criteriaBuilder.and(
                        criteriaBuilder.and(
                                predSearchTerm,
                                getPredForSortByWithRange(root, criteriaBuilder)
                        ),
                        criteriaBuilder.equal(root.get("accepted").as(Boolean.class), accepted)
                );
            } else {
                return criteriaBuilder.and(
                        predSearchTerm,
                        criteriaBuilder.equal(root.get("accepted").as(Boolean.class), accepted)
                );
            }
        }
    }
}
