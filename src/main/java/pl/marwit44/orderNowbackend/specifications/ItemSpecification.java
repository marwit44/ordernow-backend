package pl.marwit44.orderNowbackend.specifications;

import org.springframework.data.jpa.domain.Specification;
import pl.marwit44.orderNowbackend.model.ItemModel;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class ItemSpecification implements Specification<ItemModel> {

    private final String searchTerm;
    private final String sortBy;
    private final int rangeFrom;
    private final int rangeTo;

    public ItemSpecification(String searchTerm, String sortBy, int rangeFrom, int rangeTo) {
        this.searchTerm = searchTerm;
        this.sortBy = sortBy;
        this.rangeFrom = rangeFrom;
        this.rangeTo = rangeTo;
    }

    private String containsPattern(String searchTerm) {
        return "%" + searchTerm.toLowerCase() + "%";
    }

    private Predicate getPredForSortByWithRange(Root<ItemModel> root, CriteriaBuilder criteriaBuilder) {

        Predicate predGreaterEqPrice = criteriaBuilder.greaterThanOrEqualTo(root.get("price"), rangeFrom);
        Predicate predLessEqPrice = criteriaBuilder.lessThanOrEqualTo(root.get("price"), rangeTo);

        Predicate predGreaterEqAmount = criteriaBuilder.greaterThanOrEqualTo(root.get("amount"), rangeFrom);
        Predicate predLessEqAmount = criteriaBuilder.lessThanOrEqualTo(root.get("amount"), rangeTo);

        if (rangeFrom > -1 && rangeTo > -1 && !sortBy.equals("name")) {
            if (sortBy.equals("price")) {
                return criteriaBuilder.and(
                        predGreaterEqPrice,
                        predLessEqPrice
                );
            } else {
                return criteriaBuilder.and(
                        predGreaterEqAmount,
                        predLessEqAmount);
            }
        } else if (rangeFrom > -1 && !sortBy.equals("name")) {
            if (sortBy.equals("price")) {
                return predGreaterEqPrice;
            } else {
                return predGreaterEqAmount;
            }
        } else if (rangeTo > -1 && !sortBy.equals("name")) {
            if (sortBy.equals("price")) {
                return predLessEqPrice;
            } else {
                return predLessEqAmount;
            }
        } else {
            return criteriaBuilder.and();
        }
    }

    @Override
    public Predicate toPredicate(Root<ItemModel> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if (searchTerm == null) {
            return getPredForSortByWithRange(root,criteriaBuilder);
        }

        Predicate predSearchTerm = criteriaBuilder.or(
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("name").as(String.class)), containsPattern(searchTerm)
                ),
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("price").as(String.class)), containsPattern(searchTerm)
                ),
                criteriaBuilder.like(
                        criteriaBuilder.lower(root.get("amount").as(String.class)), containsPattern(searchTerm)
                )
        );

        if (!sortBy.equals("name")) {
            return criteriaBuilder.and(
                        predSearchTerm,
                        getPredForSortByWithRange(root,criteriaBuilder)
                    );
        } else {
            return predSearchTerm;
        }
    }
}
