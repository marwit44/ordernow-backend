package pl.marwit44.orderNowbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.marwit44.orderNowbackend.model.ItemDetailsModel;

@Repository
public interface ItemDetailsRepository extends JpaRepository<ItemDetailsModel, Long> {
}
