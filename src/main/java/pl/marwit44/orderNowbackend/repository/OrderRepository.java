package pl.marwit44.orderNowbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.marwit44.orderNowbackend.model.OrderModel;

import java.util.List;

@Repository
public interface OrderRepository extends JpaRepository<OrderModel, Long>, JpaSpecificationExecutor<OrderModel> {

    OrderModel getOneByUserIdAndAccepted(Long userId, Boolean accepted);

    List<OrderModel> findByUserId(Long userId);
}
