package pl.marwit44.orderNowbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.marwit44.orderNowbackend.model.ImageModel;

@Repository
public interface ImageRepository extends JpaRepository<ImageModel, Long> {
}
