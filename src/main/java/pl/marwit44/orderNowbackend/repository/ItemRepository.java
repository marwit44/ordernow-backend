package pl.marwit44.orderNowbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.marwit44.orderNowbackend.model.ItemModel;

import java.util.Optional;

@Repository
public interface ItemRepository extends JpaRepository<ItemModel, Long>, JpaSpecificationExecutor<ItemModel> {
    ItemModel getOneByName(String name);

    Optional<ItemModel> findOneByName(String name);
}
