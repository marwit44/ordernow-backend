package pl.marwit44.orderNowbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;
import pl.marwit44.orderNowbackend.model.ItemInOrderModel;

import java.util.List;

@Repository
public interface ItemInOrderRepository extends JpaRepository<ItemInOrderModel, Long>, JpaSpecificationExecutor<ItemInOrderModel> {

    ItemInOrderModel getOneByOrderIdAndNameAndPrice(Long orderId, String name, double price);

    List<ItemInOrderModel> findAllByName(String name);
}
