package pl.marwit44.orderNowbackend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.marwit44.orderNowbackend.dto.Item;
import pl.marwit44.orderNowbackend.dto.ItemInOrder;
import pl.marwit44.orderNowbackend.exceptions.ItemAmountNotEnoughException;
import pl.marwit44.orderNowbackend.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowbackend.model.ItemDetailsModel;
import pl.marwit44.orderNowbackend.model.ItemInOrderModel;
import pl.marwit44.orderNowbackend.model.ItemModel;
import pl.marwit44.orderNowbackend.model.OrderModel;
import pl.marwit44.orderNowbackend.model.OrderStatus;
import pl.marwit44.orderNowbackend.repository.ItemDetailsRepository;
import pl.marwit44.orderNowbackend.repository.ItemInOrderRepository;
import pl.marwit44.orderNowbackend.repository.ItemRepository;
import pl.marwit44.orderNowbackend.repository.OrderRepository;
import pl.marwit44.orderNowbackend.specifications.ItemSpecification;
import pl.marwit44.orderNowbackend.transformer.ItemInOrderTransformer;
import pl.marwit44.orderNowbackend.transformer.ItemTransformer;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ItemService {

    private final Logger log = LoggerFactory.getLogger(ItemService.class);

    private final ItemRepository itemRepository;
    private final OrderRepository orderRepository;
    private final ItemInOrderRepository itemInOrderRepository;
    private final ItemDetailsRepository itemDetailsRepository;

    @Autowired
    public ItemService(ItemRepository itemRepository, OrderRepository orderRepository, ItemInOrderRepository itemInOrderRepository, ItemDetailsRepository itemDetailsRepository) {
        this.itemRepository = itemRepository;
        this.orderRepository = orderRepository;
        this.itemInOrderRepository = itemInOrderRepository;
        this.itemDetailsRepository = itemDetailsRepository;
    }

    public List<Item> getAllItemsWithPaginationAndFilter(int pageNumber, String searchTerm, String sortBy, boolean desc, int rangeFrom, int rangeTo) {

        ItemSpecification itemSpecification = new ItemSpecification(searchTerm, sortBy, rangeFrom, rangeTo);

        long allItemsSizePages = (itemRepository.count(itemSpecification) - 1) / 8 + 1;

        return getItems(pageNumber, sortBy, desc, itemSpecification, (int) allItemsSizePages);
    }

    private List<Item> getItems(int pageNumber, String sortBy, boolean desc, ItemSpecification itemSpecification, int allItemsSizePages) {
        if (pageNumber > allItemsSizePages) {
            pageNumber = allItemsSizePages;
        }

        if (pageNumber <= 0) {
            pageNumber = 1;
        }

        PageRequest pageable;

        if (!desc) {
            pageable = PageRequest.of(pageNumber - 1, 8, Sort.by(sortBy).and(Sort.by("name")));
        } else {
            pageable = PageRequest.of(pageNumber - 1, 8, Sort.by(sortBy).descending().and(Sort.by("name")));
        }

        Page<ItemModel> all;
        if (itemSpecification == null) {
            all = itemRepository.findAll(pageable);
        } else {
            all = itemRepository.findAll(itemSpecification, pageable);
        }

        List<Item> itemList = all.getContent().stream()
                .map(ItemTransformer::getItemDtoFromModel)
                .collect(Collectors.toList());

        if (itemList.size() > 0) {
            itemList.get(0).setTotalPages(all.getTotalPages());
        }

        return itemList;
    }

    private ItemModel getOneById(Long id) {
        return itemRepository.getOne(id);
    }

    public Item getById(Long id) throws ItemNotFoundException {

        Optional<ItemModel> itemOptional = itemRepository.findById(id);

        if (itemOptional.isPresent()) {
            return ItemTransformer.getItemDtoFromModel(itemOptional.get());
        } else {
            throw new ItemNotFoundException("Item not found id: " + id);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void saveItem(Item item) {
        try {
            if (item.getId() != null) {
                itemRepository.save(ItemTransformer.getItemModelFromDto(item, getOneById(item.getId())));
            } else {
                itemRepository.save(ItemTransformer.getItemModelFromDto(item, new ItemModel()));
                itemDetailsRepository.save(new ItemDetailsModel());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void deleteItem(Item item) {
        try {
            itemRepository.delete(ItemTransformer.getItemModelFromDto(item, getOneById(item.getId())));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void buyItem(Item item, Long userId) throws ItemAmountNotEnoughException {
        try {
            if (item.getId() != null) {

                ItemModel itemModel = getOneById(item.getId());

                if (itemModel.getAmount() >= item.getAmount() && item.getAmount() > 0) {

                    List<OrderModel> allOrders = orderRepository.findByUserId(userId);

                    OrderModel orderModel;

                    if (allOrders.size() == 0) {
                        orderModel = new OrderModel();

                    } else {
                        if (allOrders.stream().anyMatch(e -> !e.isAccepted())) {

                            Optional<OrderModel> orderOptional = allOrders.stream()
                                    .filter(e -> !e.isAccepted())
                                    .findFirst();

                            orderModel = orderOptional.orElseGet(OrderModel::new);

                        } else {
                            orderModel = new OrderModel();
                        }
                    }
                    saveOrderModel(item, orderModel, userId);

                    item.setAmount(itemModel.getAmount() - item.getAmount());

                    itemRepository.save(ItemTransformer.getItemModelFromDto(item, itemModel));
                } else {

                    if (item.getAmount() > 0) {
                        throw new ItemAmountNotEnoughException("Item amount not enough");
                    }
                }

            } else {
                itemRepository.save(ItemTransformer.getItemModelFromDto(item, new ItemModel()));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    private void saveOrderModel(Item item, OrderModel orderModel, Long userId) {

        try {
            if (orderModel.getPrice() == null || orderModel.getPrice() == 0) {
                orderModel.setPrice(item.getPrice() * item.getAmount());
            } else {
                orderModel.setPrice(orderModel.getPrice() + item.getPrice() * item.getAmount());
            }

            orderModel.setUserId(userId);
            orderModel.setStatus(OrderStatus.PREPARATION);

            ItemInOrderModel itemInOrderModel = itemInOrderRepository.getOneByOrderIdAndNameAndPrice(orderModel.getId(), item.getName(), item.getPrice());

            if (itemInOrderModel != null) {
                itemInOrderModel.setAmount(itemInOrderModel.getAmount() + item.getAmount());

                itemInOrderRepository.save(itemInOrderModel);

            } else {
                itemInOrderModel = ItemInOrderTransformer.getItemInOrderModelFromItemDto(item);

                List<ItemInOrderModel> itemList;

                if (orderModel.getItemList() != null) {
                    itemList = orderModel.getItemList();

                } else {
                    itemList = new ArrayList<>();
                }
                itemList.add(itemInOrderModel);

                orderModel.setItemList(itemList);

                itemInOrderModel.setOrder(orderModel);
            }

            orderRepository.save(orderModel);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void getBackItem(ItemInOrder itemInOrder) {

        ItemModel itemModel = itemRepository.getOneByName(itemInOrder.getName());

        if (itemModel != null) {
            itemModel.setAmount(itemModel.getAmount() + itemInOrder.getAmount());

        } else {

            itemModel = ItemTransformer.getItemModelFromItemInOrder(itemInOrder, new ItemModel());

        }
        itemRepository.save(itemModel);
    }

    public Item getByName(String name) throws ItemNotFoundException {
        try {
            Optional<ItemModel> itemOptional = itemRepository.findOneByName(name);

            if (itemOptional.isPresent()) {
                return ItemTransformer.getItemDtoFromModel(itemOptional.get());
            } else {
                throw new ItemNotFoundException("Item not found name: " + name);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            return new Item();
        }
    }
}
