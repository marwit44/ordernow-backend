package pl.marwit44.orderNowbackend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.marwit44.orderNowbackend.dto.ItemInOrder;
import pl.marwit44.orderNowbackend.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowbackend.model.ItemInOrderModel;
import pl.marwit44.orderNowbackend.model.OrderModel;
import pl.marwit44.orderNowbackend.model.OrderStatus;
import pl.marwit44.orderNowbackend.repository.ItemInOrderRepository;
import pl.marwit44.orderNowbackend.repository.OrderRepository;
import pl.marwit44.orderNowbackend.specifications.ItemInOrderSpecification;
import pl.marwit44.orderNowbackend.transformer.ItemInOrderTransformer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ItemInOrderService {

    private final Logger log = LoggerFactory.getLogger(ItemInOrderService.class);

    private final ItemInOrderRepository itemInOrderRepository;
    private final OrderRepository orderRepository;

    @Autowired
    public ItemInOrderService(ItemInOrderRepository itemInOrderRepository, OrderRepository orderRepository) {
        this.itemInOrderRepository = itemInOrderRepository;
        this.orderRepository = orderRepository;
    }

    public List<ItemInOrder> getItemsInOrderListWithPaginationAndFilter(long id, int pageNumber, String searchTerm, String sortBy, boolean desc, int rangeFrom, int rangeTo) {

        try {
            ItemInOrderSpecification itemInOrderSpecification = new ItemInOrderSpecification(searchTerm, id, sortBy, rangeFrom, rangeTo);

            long allItemsInOrderSizePages = (itemInOrderRepository.findAll(itemInOrderSpecification).size() - 1) / 8 + 1;

            return getItemsInOrders(pageNumber, itemInOrderSpecification, (int) allItemsInOrderSizePages, sortBy, desc);

        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return new ArrayList<>();
        }
    }

    private List<ItemInOrder> getItemsInOrders(int pageNumber, ItemInOrderSpecification itemInOrderSpecification, int allItemsInOrderSizePages, String sortBy, boolean desc) {
        if (pageNumber > allItemsInOrderSizePages) {
            pageNumber = allItemsInOrderSizePages;
        }

        if (pageNumber <= 0) {
            pageNumber = 1;
        }

        PageRequest pageable;

        if (!desc) {
            pageable = PageRequest.of(pageNumber - 1, 8, Sort.by(sortBy).and(Sort.by("name")));
        } else {
            pageable = PageRequest.of(pageNumber - 1, 8, Sort.by(sortBy).descending().and(Sort.by("name")));
        }

        Page<ItemInOrderModel> all;

        if (itemInOrderSpecification == null) {
            all = itemInOrderRepository.findAll(pageable);
        } else {
            all = itemInOrderRepository.findAll(itemInOrderSpecification, pageable);
        }

        List<ItemInOrder> itemInOrderList = all.getContent().stream()
                .map(ItemInOrderTransformer::getItemInOrderDtoFromModel)
                .collect(Collectors.toList());

        if (itemInOrderList.size() > 0) {
            itemInOrderList.get(0).setTotalPages(all.getTotalPages());
        }

        return itemInOrderList;
    }

    public ItemInOrder getById(Long id) throws ItemNotFoundException {
        Optional<ItemInOrderModel> itemOptional = itemInOrderRepository.findById(id);

        if (itemOptional.isPresent()) {
            return ItemInOrderTransformer.getItemInOrderDtoFromModel(itemOptional.get());
        } else {
            throw new ItemNotFoundException("Item not found id: " + id);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void deleteItemFromOrder(ItemInOrder itemInOrder, Long orderId) {

        try {
            OrderModel order = orderRepository.getOne(orderId);

            List<ItemInOrderModel> itemInOrderList = order.getItemList();

            for (Iterator<ItemInOrderModel> iterator = itemInOrderList.iterator(); iterator.hasNext(); ) {
                ItemInOrderModel itemInOrderFromList = iterator.next();
                if (itemInOrderFromList.getName().equals(itemInOrder.getName()) && itemInOrderFromList.getAmount() == itemInOrder.getAmount()) {
                    iterator.remove();
                    itemInOrderRepository.deleteById(itemInOrder.getId());
                } else if (itemInOrderFromList.getName().equals(itemInOrder.getName())) {
                    itemInOrderFromList.setAmount(itemInOrderFromList.getAmount() - itemInOrder.getAmount());
                }
            }

            order.setItemList(itemInOrderList);
            order.setPrice(order.getPrice() - itemInOrder.getAmount() * itemInOrder.getPrice());

            if (order.getItemList().size() != 0) {
                orderRepository.save(order);
            } else {

                orderRepository.deleteById(order.getId());
            }

        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    public boolean checkAllWithName(String name) {

        List<ItemInOrderModel> itemInOrderList = itemInOrderRepository.findAllByName(name);

        for (ItemInOrderModel itemInOrderModel : itemInOrderList) {
            OrderModel order = itemInOrderModel.getOrder();

            if (order != null) {
                OrderStatus status = order.getStatus();
                if (status != OrderStatus.DONE) {
                    return false;
                }
            }
        }

        return true;
    }
}

