package pl.marwit44.orderNowbackend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.marwit44.orderNowbackend.dto.Order;
import pl.marwit44.orderNowbackend.exceptions.OrderNotFoundException;
import pl.marwit44.orderNowbackend.model.OrderModel;
import pl.marwit44.orderNowbackend.model.UserRole;
import pl.marwit44.orderNowbackend.repository.OrderRepository;
import pl.marwit44.orderNowbackend.specifications.OrderSpecification;
import pl.marwit44.orderNowbackend.transformer.OrderTransformer;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrderService {

    private final Logger log = LoggerFactory.getLogger(OrderService.class);

    private final OrderRepository orderRepository;

    @Autowired
    public OrderService(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    public List<Order> getAllOrdersWithPaginationAndFilter(Long userId, UserRole role, int pageNumber, String searchTerm, String sortBy, boolean desc, long rangeFromVal, long rangeToVal, LocalDateTime rangeFromDateTime, LocalDateTime rangeToDateTime) {

        OrderSpecification orderSpecification = new OrderSpecification(searchTerm, userId, true, role, sortBy, rangeFromVal, rangeToVal, rangeFromDateTime, rangeToDateTime);

        long allOrdersSizePages = (orderRepository.findAll(orderSpecification).size() - 1) / 8 + 1;

        return getOrders(pageNumber, sortBy, desc, orderSpecification, (int) allOrdersSizePages);
    }

    private List<Order> getOrders(int pageNumber, String sortBy, boolean desc, OrderSpecification orderSpecification, int allOrdersSizePages) {
        if (pageNumber > allOrdersSizePages) {
            pageNumber = allOrdersSizePages;
        }

        if (pageNumber <= 0) {
            pageNumber = 1;
        }

        PageRequest pageable;

        if (!desc) {
            pageable = PageRequest.of(pageNumber - 1, 8, Sort.by(sortBy).and(Sort.by("addDate")));
        } else {
            pageable = PageRequest.of(pageNumber - 1, 8, Sort.by(sortBy).descending().and(Sort.by("addDate")));
        }

        Page<OrderModel> all;
        if (orderSpecification == null) {
            all = orderRepository.findAll(pageable);
        } else {
            all = orderRepository.findAll(orderSpecification, pageable);
        }

        List<Order> orderList = all.getContent().stream()
                .map(OrderTransformer::getOrderDtoFromModelWithItemList)
                .collect(Collectors.toList());

        if (orderList.size() > 0) {
            orderList.get(0).setTotalPages(all.getTotalPages());
        }

        return orderList;
    }

    private OrderModel getOneById(Long id) {
        return orderRepository.getOne(id);
    }


    public Order getNotAccepted(Long userId) {
        return OrderTransformer.getOrderDtoFromModelWithItemList(orderRepository.getOneByUserIdAndAccepted(userId, false));
    }

    public Order getById(Long id) throws OrderNotFoundException {
        Optional<OrderModel> orderOptional = orderRepository.findById(id);

        if (orderOptional.isPresent()) {
            return OrderTransformer.getOrderDtoFromModelWithItemList(orderOptional.get());
        } else {
            throw new OrderNotFoundException("Order not found id: " + id);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void deleteOrder(Order order) {

        try {
            orderRepository.deleteById(order.getId());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void editOrder(Order order) {
        try {
            orderRepository.save(OrderTransformer.getOrderModelFromDtoWithItemList(order, getOneById(order.getId())));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void acceptOrder(Order order) {

        try {
            order.setAccepted(true);

            orderRepository.save(OrderTransformer.getOrderModelFromDtoWithItemList(order, getOneById(order.getId())));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
