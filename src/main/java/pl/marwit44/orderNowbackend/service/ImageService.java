package pl.marwit44.orderNowbackend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.marwit44.orderNowbackend.dto.Image;
import pl.marwit44.orderNowbackend.model.ImageModel;
import pl.marwit44.orderNowbackend.repository.ImageRepository;
import pl.marwit44.orderNowbackend.transformer.ImageTransformer;

@Service
public class ImageService {

    private final Logger log = LoggerFactory.getLogger(ImageService.class);

    private final ImageRepository imageRepository;

    @Autowired
    public ImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    private ImageModel getOneById(Long id) {
        return imageRepository.getOne(id);
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void saveImage(Image image) {

        try {
            if (image.getId() != null) {
                imageRepository.save(ImageTransformer.getImageModelFromDto(image, getOneById(image.getId())));
            } else {
                imageRepository.save(ImageTransformer.getImageModelFromDto(image, new ImageModel()));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void deleteImage(Image image) {

        try {
            if (image != null) {
                imageRepository.delete(ImageTransformer.getImageModelFromDto(image, getOneById(image.getId())));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }
}
