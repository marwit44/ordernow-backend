package pl.marwit44.orderNowbackend.service;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import pl.marwit44.orderNowbackend.Constants;
import pl.marwit44.orderNowbackend.dto.Token;
import pl.marwit44.orderNowbackend.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowbackend.model.UserRole;

import java.io.UnsupportedEncodingException;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.util.Date;

@Service
public class AuthorizationService {

    private final Logger log = LoggerFactory.getLogger(AuthorizationService.class);

    String generateToken(LocalDateTime dateTime) {

        String jwtKey = Constants.JWT_TOKEN_KEY_USER;

        try {
            Algorithm algorithm = Algorithm.HMAC256(jwtKey);
            Date expirationDate = Date.from(ZonedDateTime.now().plusHours(24).toInstant());
            Date issuedAt = Date.from(ZonedDateTime.now().toInstant());
            return JWT.create()
                    .withIssuedAt(issuedAt)
                    .withExpiresAt(expirationDate)
                    .withClaim("id", 1)
                    .withClaim("dateTime", dateTime.toString())
                    .withIssuer("jwtauth")
                    .sign(algorithm);
        } catch (UnsupportedEncodingException | JWTCreationException e) {
            log.error(e.getMessage(), e);
        }
        return "";
    }

    private Token validateToken(String token, UserRole role) {
        try {
            if(token != null) {

                String jwtKey = "";

                if(role == UserRole.ROLE_USER) {
                    jwtKey = Constants.JWT_TOKEN_KEY_USER;
                } else if(role == UserRole.ROLE_ADMIN) {
                    jwtKey = Constants.JWT_TOKEN_KEY_ADMIN;
                }

                Algorithm algorithm = Algorithm.HMAC256(jwtKey);
                JWTVerifier verifier = JWT.require(algorithm)
                        .withIssuer("jwtauth")
                        .build();
                DecodedJWT jwt = verifier.verify(token);

                Claim id = jwt.getClaim("id");
                Claim dateTime = jwt.getClaim("dateTime");

                LocalDateTime sentRestTime = LocalDateTime.parse(dateTime.asString());

                return Token.Builder.aToken()
                        .withId(id.asLong())
                        .withDateTime(sentRestTime)
                        .build();
            }
        } catch (UnsupportedEncodingException | JWTVerificationException e) {
            log.error(e.getMessage(), e);
        }
        return null;
    }

    public Boolean compareTokens(Token tokenSent) throws TokenAuthorizationException {

        LocalDateTime sentRestTime = LocalDateTime.now();

        if(tokenSent != null) {
            Token tokenReturned = validateToken(tokenSent.getValue(), tokenSent.getRole());

            if(tokenReturned != null) {
                if(tokenReturned.getId().equals(tokenSent.getId()) && tokenReturned.getDateTime().plusSeconds(2).isAfter(sentRestTime)) {
                    return true;
                }
            }
        }

        throw new TokenAuthorizationException("Authorization token not valid");
    }
}
