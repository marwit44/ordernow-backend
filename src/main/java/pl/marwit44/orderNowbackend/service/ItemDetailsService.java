package pl.marwit44.orderNowbackend.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import pl.marwit44.orderNowbackend.dto.ItemDetails;
import pl.marwit44.orderNowbackend.exceptions.ItemDetailsNotFoundException;
import pl.marwit44.orderNowbackend.model.ItemDetailsModel;
import pl.marwit44.orderNowbackend.repository.ItemDetailsRepository;
import pl.marwit44.orderNowbackend.transformer.ItemDetailsTransformer;

import java.util.Optional;

@Service
public class ItemDetailsService {

    private final Logger log = LoggerFactory.getLogger(ItemDetailsService.class);

    private final ItemDetailsRepository itemDetailsRepository;

    @Autowired
    public ItemDetailsService(ItemDetailsRepository itemDetailsRepository) {
        this.itemDetailsRepository = itemDetailsRepository;
    }

    public ItemDetails getById(Long id) throws ItemDetailsNotFoundException {
        Optional<ItemDetailsModel> itemDetailsOptional = itemDetailsRepository.findById(id);

        if (itemDetailsOptional.isPresent()) {
            return ItemDetailsTransformer.getItemDetailsDtoFromModelWithImageList(itemDetailsOptional.get());
        } else {
            throw new ItemDetailsNotFoundException("Item details not found id: " + id);
        }
    }

    @Transactional(propagation = Propagation.REQUIRES_NEW, rollbackFor = Exception.class)
    public void editDetails(ItemDetails itemDetails) {
        try {
            if (itemDetails.getId() != null) {
                itemDetailsRepository.save(ItemDetailsTransformer.getItemDetailsModelFromDtoWithItemList(itemDetails, getOneById(itemDetails.getId())));
            } else {
                itemDetailsRepository.save(ItemDetailsTransformer.getItemDetailsModelFromDtoWithItemList(itemDetails, new ItemDetailsModel()));
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private ItemDetailsModel getOneById(Long id) {
        return itemDetailsRepository.getOne(id);
    }
}
