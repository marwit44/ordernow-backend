package pl.marwit44.orderNowbackend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.marwit44.orderNowbackend.dto.Image;
import pl.marwit44.orderNowbackend.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowbackend.service.AuthorizationService;
import pl.marwit44.orderNowbackend.service.ImageService;

@RestController
@RequestMapping("/image")
public class ImageController {

    private final Logger log = LoggerFactory.getLogger(ImageController.class);

    private final ImageService imageService;
    private final AuthorizationService authorizationService;

    @Autowired
    public ImageController(ImageService imageService, AuthorizationService authorizationService) {
        this.imageService = imageService;
        this.authorizationService = authorizationService;
    }

    @PostMapping("/save")
    public ResponseEntity saveItem(@RequestBody Image image) {

        try {
            if (authorizationService.compareTokens(image.getToken())) {
                imageService.saveImage(image);
                return ResponseEntity.ok().build();
            }
        } catch (Exception | TokenAuthorizationException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/delete")
    public ResponseEntity deleteItem(@RequestBody Image image) {

        try {
            if (authorizationService.compareTokens(image.getToken())) {
                imageService.deleteImage(image);
                return ResponseEntity.ok().build();
            }

        } catch (Exception | TokenAuthorizationException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.badRequest().build();
    }
}
