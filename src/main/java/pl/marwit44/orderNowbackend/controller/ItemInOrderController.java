package pl.marwit44.orderNowbackend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.marwit44.orderNowbackend.dto.Item;
import pl.marwit44.orderNowbackend.dto.ItemInOrder;
import pl.marwit44.orderNowbackend.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowbackend.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowbackend.service.AuthorizationService;
import pl.marwit44.orderNowbackend.service.ItemInOrderService;
import pl.marwit44.orderNowbackend.service.ItemService;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@RestController
@RequestMapping("/itemInOrder")
public class ItemInOrderController {


    private final Logger log = LoggerFactory.getLogger(ItemInOrderController.class);

    private final ItemInOrderService itemInOrderService;
    private final ItemService itemService;
    private final AuthorizationService authorizationService;

    @Autowired
    public ItemInOrderController(ItemInOrderService itemInOrderService, ItemService itemService, AuthorizationService authorizationService) {
        this.itemInOrderService = itemInOrderService;
        this.itemService = itemService;
        this.authorizationService = authorizationService;
    }

    @GetMapping("/getList")
    @Produces("application/json")
    public ResponseEntity getAll(@RequestParam(name = "id") Long id, @RequestParam(name = "page") int page, @RequestParam(name = "sortBy") String sortBy, @RequestParam(name = "desc") boolean desc, @RequestParam(name = "rangeFrom") int rangeFrom, @RequestParam(name = "rangeTo") int rangeTo) {

        return new ResponseEntity<>(itemInOrderService.getItemsInOrderListWithPaginationAndFilter(id, page, null, sortBy, desc, rangeFrom, rangeTo), HttpStatus.OK);
    }

    @GetMapping("/filtering")
    @Produces("application/json")
    public ResponseEntity filterByName(@RequestParam(name = "id") Long id, @RequestParam(name = "name") String name, @RequestParam(name = "page") int page, @RequestParam(name = "sortBy") String sortBy, @RequestParam(name = "desc") boolean desc, @RequestParam(name = "rangeFrom") int rangeFrom, @RequestParam(name = "rangeTo") int rangeTo) throws UnsupportedEncodingException {

        return new ResponseEntity<>(itemInOrderService.getItemsInOrderListWithPaginationAndFilter(id, page, URLDecoder.decode(name, "UTF-8"), sortBy, desc, rangeFrom, rangeTo), HttpStatus.OK);
    }

    @GetMapping("/getById")
    @Produces("application/json")
    public ResponseEntity getById(@RequestParam(name = "id") Long id) {

        try {
            return new ResponseEntity<>(itemInOrderService.getById(id), HttpStatus.OK);

        } catch (ItemNotFoundException e) {

            log.error(e.getMessage(), e);
            return new ResponseEntity<>(new ItemInOrder(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/checkAllWithName")
    @Produces("application/json")
    public ResponseEntity checkAllWithName(@RequestParam(name = "name") String name) {

        try {
            return new ResponseEntity<>(itemInOrderService.checkAllWithName(URLDecoder.decode(name, "UTF-8")), HttpStatus.OK);
        } catch (UnsupportedEncodingException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/delete")
    @Produces("application/json")
    public ResponseEntity deleteItemFromOrder(@RequestParam(name = "orderId") Long orderId, @RequestBody ItemInOrder itemInOrder) {

        try {
            if (authorizationService.compareTokens(itemInOrder.getToken())) {
                itemService.getBackItem(itemInOrder);

                itemInOrderService.deleteItemFromOrder(itemInOrder, orderId);

                return ResponseEntity.ok().build();
            }

        } catch (TokenAuthorizationException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(new Item(), HttpStatus.FORBIDDEN);
        }

        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(Exception.class)
    @Produces("application/json")
    public ResponseEntity handleError(HttpServletRequest req, Exception ex) {

        return new ResponseEntity<>(ex.getMessage(), HttpStatus.OK);
    }
}
