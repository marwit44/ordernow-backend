package pl.marwit44.orderNowbackend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.marwit44.orderNowbackend.dto.ItemInOrder;
import pl.marwit44.orderNowbackend.dto.Order;
import pl.marwit44.orderNowbackend.exceptions.OrderNotFoundException;
import pl.marwit44.orderNowbackend.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowbackend.model.UserRole;
import pl.marwit44.orderNowbackend.service.AuthorizationService;
import pl.marwit44.orderNowbackend.service.ItemService;
import pl.marwit44.orderNowbackend.service.OrderService;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.time.LocalDateTime;

@RestController
@RequestMapping("/order")
public class OrderController {


    private final Logger log = LoggerFactory.getLogger(OrderController.class);

    private final OrderService orderService;
    private final ItemService itemService;
    private final AuthorizationService authorizationService;

    @Autowired
    public OrderController(OrderService orderService, ItemService itemService, AuthorizationService authorizationService) {
        this.orderService = orderService;
        this.itemService = itemService;
        this.authorizationService = authorizationService;
    }

    @GetMapping("/getAll")
    @Produces("application/json")
    public ResponseEntity getAll(@RequestParam(name = "id") Long id, @RequestParam(name = "role") UserRole role, @RequestParam(name = "page") int page, @RequestParam(name = "sortBy") String sortBy, @RequestParam(name = "desc") boolean desc, @RequestParam(name = "rangeFrom") long rangeFrom, @RequestParam(name = "rangeTo") long rangeTo, @RequestParam(name = "rangeFromDateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime rangeFromDateTime, @RequestParam(name = "rangeToDateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime rangeToDateTime) {

        return new ResponseEntity<>(orderService.getAllOrdersWithPaginationAndFilter(id, role, page, null, sortBy, desc, rangeFrom, rangeTo, rangeFromDateTime, rangeToDateTime), HttpStatus.OK);
    }

    @GetMapping("/filtering")
    @Produces("application/json")
    public ResponseEntity filterByName(@RequestParam(name = "id") Long id, @RequestParam(name = "role") UserRole role, @RequestParam(name = "name") String name, @RequestParam(name = "page") int page, @RequestParam(name = "sortBy") String sortBy, @RequestParam(name = "desc") boolean desc, @RequestParam(name = "rangeFrom") long rangeFrom, @RequestParam(name = "rangeTo") long rangeTo, @RequestParam(name = "rangeFromDateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime rangeFromDateTime, @RequestParam(name = "rangeToDateTime") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime rangeToDateTime) throws UnsupportedEncodingException {

        return new ResponseEntity<>(orderService.getAllOrdersWithPaginationAndFilter(id, role, page, URLDecoder.decode(name, "UTF-8"), sortBy, desc, rangeFrom, rangeTo, rangeFromDateTime, rangeToDateTime), HttpStatus.OK);
    }

    @GetMapping("/getById")
    @Produces("application/json")
    public ResponseEntity getById(@RequestParam(name = "id") Long id) {

        try {
            return new ResponseEntity<>(orderService.getById(id), HttpStatus.OK);

        } catch (OrderNotFoundException e) {

            log.error(e.getMessage(), e);
            return new ResponseEntity<>(new Order(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getNotAccepted")
    @Produces("application/json")
    public ResponseEntity getNotAccepted(@RequestParam(name = "userId") Long userId) {

        return new ResponseEntity<>(orderService.getNotAccepted(userId), HttpStatus.OK);
    }

    @PostMapping("/edit")
    public ResponseEntity editItem(@RequestBody Order order) {

        try {
            if (authorizationService.compareTokens(order.getToken())) {
                orderService.editOrder(order);

                return ResponseEntity.ok().build();
            }

        } catch (TokenAuthorizationException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/delete")
    public ResponseEntity deleteItem(@RequestBody Order order) {

        try {
            if (authorizationService.compareTokens(order.getToken())) {
                for (ItemInOrder item : order.getItemList()) {
                    itemService.getBackItem(item);
                }

                orderService.deleteOrder(order);

                return ResponseEntity.ok().build();
            }

        } catch (TokenAuthorizationException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/accept")
    public ResponseEntity acceptItem(@RequestBody Order order) {

        try {
            if (authorizationService.compareTokens(order.getToken())) {
                orderService.acceptOrder(order);

                return ResponseEntity.ok().build();
            }

        } catch (TokenAuthorizationException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(Exception.class)
    @Produces("application/json")
    public ResponseEntity handleError(HttpServletRequest req, Exception ex) {

        return new ResponseEntity<>(ex.getMessage(), HttpStatus.OK);
    }
}
