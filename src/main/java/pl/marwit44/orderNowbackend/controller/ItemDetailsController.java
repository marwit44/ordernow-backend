package pl.marwit44.orderNowbackend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.marwit44.orderNowbackend.dto.ItemDetails;
import pl.marwit44.orderNowbackend.exceptions.ItemDetailsNotFoundException;
import pl.marwit44.orderNowbackend.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowbackend.service.AuthorizationService;
import pl.marwit44.orderNowbackend.service.ItemDetailsService;

import javax.ws.rs.Produces;

@RestController
@RequestMapping("/itemDetails")
public class ItemDetailsController {

    private final Logger log = LoggerFactory.getLogger(ItemDetailsController.class);

    private final ItemDetailsService itemDetailsService;
    private final AuthorizationService authorizationService;

    @Autowired
    public ItemDetailsController(ItemDetailsService itemDetailsService, AuthorizationService authorizationService) {
        this.itemDetailsService = itemDetailsService;
        this.authorizationService = authorizationService;
    }

    @GetMapping("/getById")
    @Produces("application/json")
    public ResponseEntity getById(@RequestParam(name = "id") Long id) {

        try {
            return new ResponseEntity<>(itemDetailsService.getById(id), HttpStatus.OK);

        } catch (ItemDetailsNotFoundException e) {

            log.error(e.getMessage(), e);
            return new ResponseEntity<>(new ItemDetails(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/save")
    public ResponseEntity saveItemDetails(@RequestBody ItemDetails itemDetails) {

        try {
            if (authorizationService.compareTokens(itemDetails.getToken())) {
                itemDetailsService.editDetails(itemDetails);

                return ResponseEntity.ok().build();
            }

        } catch (TokenAuthorizationException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.badRequest().build();
    }
}
