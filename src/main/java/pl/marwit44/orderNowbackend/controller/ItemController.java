package pl.marwit44.orderNowbackend.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import pl.marwit44.orderNowbackend.dto.Item;
import pl.marwit44.orderNowbackend.exceptions.ItemAmountNotEnoughException;
import pl.marwit44.orderNowbackend.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowbackend.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowbackend.service.AuthorizationService;
import pl.marwit44.orderNowbackend.service.ItemService;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Produces;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

@RestController
@RequestMapping("/item")
public class ItemController {

    private final Logger log = LoggerFactory.getLogger(ItemController.class);

    private final ItemService itemService;
    private final AuthorizationService authorizationService;

    @Autowired
    public ItemController(ItemService itemService, AuthorizationService authorizationService) {
        this.itemService = itemService;
        this.authorizationService = authorizationService;
    }

    @GetMapping("/getAll")
    @Produces("application/json")
    public ResponseEntity getAll(@RequestParam(name = "page") int page, @RequestParam(name = "sortBy") String sortBy, @RequestParam(name = "desc") boolean desc, @RequestParam(name = "rangeFrom") int rangeFrom, @RequestParam(name = "rangeTo") int rangeTo) {

        return new ResponseEntity<>(itemService.getAllItemsWithPaginationAndFilter(page, null, sortBy, desc, rangeFrom, rangeTo), HttpStatus.OK);
    }

    @GetMapping("/filtering")
    @Produces("application/json")
    public ResponseEntity filterByName(@RequestParam(name = "name") String name, @RequestParam(name = "page") int page, @RequestParam(name = "sortBy") String sortBy, @RequestParam(name = "desc") boolean desc, @RequestParam(name = "rangeFrom") int rangeFrom, @RequestParam(name = "rangeTo") int rangeTo) throws UnsupportedEncodingException {

        return new ResponseEntity<>(itemService.getAllItemsWithPaginationAndFilter(page, URLDecoder.decode(name, "UTF-8"), sortBy, desc, rangeFrom, rangeTo), HttpStatus.OK);
    }

    @GetMapping("/getById")
    @Produces("application/json")
    public ResponseEntity getById(@RequestParam(name = "id") Long id) {

        try {
            return new ResponseEntity<>(itemService.getById(id), HttpStatus.OK);

        } catch (ItemNotFoundException e) {

            log.error(e.getMessage(), e);
            return new ResponseEntity<>(new Item(), HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/getByName")
    @Produces("application/json")
    public ResponseEntity getByName(@RequestParam(name = "name") String name) {

        try {
            return new ResponseEntity<>(itemService.getByName(URLDecoder.decode(name, "UTF-8")), HttpStatus.OK);

        } catch (ItemNotFoundException | UnsupportedEncodingException e) {

            log.error(e.getMessage(), e);
            return new ResponseEntity<>(new Item(), HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/save")
    public ResponseEntity saveItem(@RequestBody Item item) {

        try {
            if (authorizationService.compareTokens(item.getToken())) {

                itemService.saveItem(item);
                return ResponseEntity.ok().build();
            }

        } catch (TokenAuthorizationException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/delete")
    public ResponseEntity deleteItem(@RequestBody Item item) {

        try {
            if (authorizationService.compareTokens(item.getToken())) {

                itemService.deleteItem(item);
                return ResponseEntity.ok().build();
            }

        } catch (TokenAuthorizationException e) {
            log.error(e.getMessage(), e);
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.badRequest().build();
    }

    @PostMapping("/buy")
    public ResponseEntity buyItem(@RequestParam(name = "userId") Long userId, @RequestBody Item item) {

        try {
            if (authorizationService.compareTokens(item.getToken())) {

                itemService.buyItem(item, userId);
                return new ResponseEntity<>(new Item(), HttpStatus.OK);
            }

        } catch (TokenAuthorizationException e) {
            log.error(e.getMessage(), e);
            return new ResponseEntity<>(new Item(), HttpStatus.FORBIDDEN);
        } catch (ItemAmountNotEnoughException e) {

            log.error(e.getMessage(), e);
            return new ResponseEntity<>(new Item(), HttpStatus.BAD_REQUEST);
        }

        return ResponseEntity.badRequest().build();
    }

    @ExceptionHandler(Exception.class)
    @Produces("application/json")
    public ResponseEntity handleError(HttpServletRequest req, Exception ex) {

        return new ResponseEntity<>(ex.getMessage(), HttpStatus.OK);
    }
}
