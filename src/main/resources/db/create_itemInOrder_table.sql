CREATE TABLE ITEM_IN_ORDER(
    ID NUMERIC(19,0),
    NAME VARCHAR(255),
    PRICE NUMERIC(19,2),
    AMOUNT NUMERIC(19,0),
    ORDER_ID NUMERIC(19,0),
    ADD_DATE TIMESTAMP,
    UPDATE_DATE TIMESTAMP,
    VERSION NUMERIC DEFAULT 0 NOT NULL,

    CONSTRAINT PK_ITEM_IN_ORDER PRIMARY KEY (ID)
);
