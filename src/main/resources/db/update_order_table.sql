ALTER TABLE ORDER_TABLE ADD USER_ID NUMERIC(19,0) DEFAULT 2;
ALTER TABLE ORDER_TABLE ADD STATUS VARCHAR(255) DEFAULT 'PREPARATION';

ALTER TABLE ORDER_TABLE ADD CONSTRAINT CHECK_STATUS CHECK (STATUS IN ('PREPARATION', 'SENDING','READY'));