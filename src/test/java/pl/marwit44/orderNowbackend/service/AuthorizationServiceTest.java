package pl.marwit44.orderNowbackend.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import pl.marwit44.orderNowbackend.dto.Token;
import pl.marwit44.orderNowbackend.exceptions.TokenAuthorizationException;
import pl.marwit44.orderNowbackend.model.UserRole;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
public class AuthorizationServiceTest {

    private AuthorizationService authorizationService;

    @Before
    public void init() {

        authorizationService = new AuthorizationService();
    }

    @Test
    public void shouldValidateTokenCorrect() throws TokenAuthorizationException {

        //Given
        LocalDateTime sentTime = LocalDateTime.now();
        String tokenValue = authorizationService.generateToken(sentTime);

        Token tokenSent = Token.Builder.aToken()
                .withId(1L)
                .withValue(tokenValue)
                .withRole(UserRole.ROLE_USER)
                .build();

        //When
        Boolean result = authorizationService.compareTokens(tokenSent);

        //Then
        Assertions.assertThat(result).isTrue();
    }

    @Test
    public void shouldNotValidateTokenCorrect() {

        //Given
        LocalDateTime sentTime = LocalDateTime.now();
        String tokenValue = authorizationService.generateToken(sentTime);

        Token tokenSent = Token.Builder.aToken()
                .withId(1L)
                .withValue(tokenValue + "addSomethingExtraToToken")
                .withRole(UserRole.ROLE_USER)
                .build();

        //When
        Throwable throwable = catchThrowable(() -> authorizationService.compareTokens(tokenSent));

        //Then
        assertThat(throwable).isInstanceOf(TokenAuthorizationException.class);
    }
}