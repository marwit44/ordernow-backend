package pl.marwit44.orderNowbackend.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.marwit44.orderNowbackend.dto.Item;
import pl.marwit44.orderNowbackend.exceptions.ItemAmountNotEnoughException;
import pl.marwit44.orderNowbackend.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowbackend.model.ItemModel;
import pl.marwit44.orderNowbackend.model.OrderModel;
import pl.marwit44.orderNowbackend.model.OrderStatus;
import pl.marwit44.orderNowbackend.repository.ItemDetailsRepository;
import pl.marwit44.orderNowbackend.repository.ItemInOrderRepository;
import pl.marwit44.orderNowbackend.repository.ItemRepository;
import pl.marwit44.orderNowbackend.repository.OrderRepository;
import pl.marwit44.orderNowbackend.transformer.ItemTransformer;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ItemServiceTest {

    private ItemService itemService;

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private ItemInOrderRepository itemInOrderRepository;

    @Autowired
    private OrderRepository orderRepository;

    private ItemDetailsRepository itemDetailsRepository;

    @Before
    public void init() {

        itemService = new ItemService(itemRepository, orderRepository, itemInOrderRepository, itemDetailsRepository);

        ItemModel item = ItemModel.Builder.anItemModel()
                .withName("chalk")
                .withPrice(4.99)
                .withAmount(5)
                .withAddDate(LocalDateTime.now())
                .build();

        ItemModel item2 = ItemModel.Builder.anItemModel()
                .withName("blackboard")
                .withPrice(149.56)
                .withAmount(2)
                .withAddDate(LocalDateTime.now())
                .build();

        OrderModel order = OrderModel.Builder.anOrderModel()
                .withPrice(99.9)
                .withAccepted(false)
                .withUserId(1L)
                .withStatus(OrderStatus.PREPARATION)
                .withAddDate(LocalDateTime.now())
                .build();

        orderRepository.save(order);

        itemRepository.saveAll(Arrays.asList(item, item2));
    }

    @Test
    public void shouldGetItemsWithPagination() {

        //Given

        //When
        List<Item> itemList = itemService.getAllItemsWithPaginationAndFilter(1, null, "name", true, -1, -1);

        //Then
        Assertions.assertThat(itemList.size()).isEqualTo(2);
        Assertions.assertThat(itemList.get(0).getName().equals("chalk"));
    }

    @Test
    public void shouldGetItemsWithPaginationAndFilter() {

        //Given

        //When
        List<Item> itemList = itemService.getAllItemsWithPaginationAndFilter(1, "ch", "name", false, 0, 200);
        List<Item> itemList2 = itemService.getAllItemsWithPaginationAndFilter(1, "ch", "price", false, 0, 2);

        //Then
        Assertions.assertThat(itemList.size()).isEqualTo(1);
        Assertions.assertThat(itemList.stream().allMatch(e -> e.getName().contains("ch"))).isTrue();
        Assertions.assertThat(itemList2.size()).isEqualTo(0);
    }

    @Test
    public void shouldGetItemById() throws ItemNotFoundException {

        // Given
        List<ItemModel> itemList = itemRepository.findAll();
        ItemModel itemForTest = itemList.get(0);

        // When
        Item itemReturned = itemService.getById(itemForTest.getId());

        // Then
        Assertions.assertThat(itemReturned.getPrice() == itemForTest.getPrice());
        Assertions.assertThat(itemReturned.getName().equals(itemForTest.getName()));
    }

    @Test
    public void shouldNotGetItemById() {

        // Given

        // When
        Throwable throwable = catchThrowable(() -> itemService.getById(1111L));

        // Then
        assertThat(throwable).isInstanceOf(ItemNotFoundException.class);
    }

    @Test
    public void shouldSaveItem() {

        // Given
        Item item = Item.Builder.anItem()
                .withName("desk")
                .withPrice(99)
                .withAmount(25)
                .build();

        // When
        itemService.saveItem(item);

        List<ItemModel> itemList = itemRepository.findAll();

        // Then
        Assertions.assertThat(itemList.stream().anyMatch(e -> e.getName().equals("desk"))).isTrue();
    }

    @Test
    public void shouldEditItem() {

        // Given
        List<ItemModel> itemList = itemRepository.findAll();
        ItemModel itemForTest = itemList.get(0);

        // When
        itemForTest.setName("car");
        itemService.saveItem(ItemTransformer.getItemDtoFromModel(itemForTest));

        itemList = itemRepository.findAll();
        ItemModel itemReturned = itemList.get(0);

        // Then
        Assertions.assertThat(itemReturned.getName().equals(itemForTest.getName())).isTrue();
        Assertions.assertThat(itemReturned.getName().equals("car")).isTrue();
    }

    @Test
    public void shouldDeleteItem() throws ItemNotFoundException {

        // Given
        List<ItemModel> itemList = itemRepository.findAll();
        Item itemForTest = itemService.getById(itemList.get(0).getId());

        // When
        itemService.deleteItem(itemForTest);
        Throwable throwable = catchThrowable(() -> itemService.getById(itemForTest.getId()));

        // Then
        assertThat(throwable).isInstanceOf(ItemNotFoundException.class);
    }

    @Test
    public void shouldBuyItem() throws ItemNotFoundException, ItemAmountNotEnoughException {

        // Given
        List<ItemModel> itemList = itemRepository.findAll();
        Item itemForTest = itemService.getById(itemList.get(0).getId());

        itemForTest.setAmount(2);

        // When
        itemService.buyItem(itemForTest, 1L);

        List<OrderModel> orderList = orderRepository.findAll();
        OrderModel orderModel = orderList.get(0);

        // Then
        Assertions.assertThat(orderModel.getItemList().size()).isEqualTo(1);

        Assertions.assertThat(orderModel.getItemList().get(0).getPrice() == itemForTest.getPrice());
        Assertions.assertThat(orderModel.getItemList().get(0).getName().equals(itemForTest.getName()));
    }

    @Test
    public void shouldNotBuyItem() throws ItemNotFoundException {

        // Given
        List<ItemModel> itemList = itemRepository.findAll();
        Item itemForTest = itemService.getById(itemList.get(0).getId());

        itemForTest.setAmount(1000);

        // When
        Throwable throwable = catchThrowable(() -> itemService.buyItem(itemForTest, 1L));

        assertThat(throwable).isInstanceOf(ItemAmountNotEnoughException.class);
    }

    @Test
    public void shouldGetItemByName() throws ItemNotFoundException {

        // Given
        List<ItemModel> itemList = itemRepository.findAll();
        ItemModel itemForTest = itemList.get(0);

        // When
        Item itemReturned = itemService.getByName(itemForTest.getName());

        // Then
        Assertions.assertThat(itemReturned.getPrice() == itemForTest.getPrice());
        Assertions.assertThat(itemReturned.getName().equals(itemForTest.getName()));
        Assertions.assertThat(itemReturned.getName().equals("chalk"));
    }
}