package pl.marwit44.orderNowbackend.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.marwit44.orderNowbackend.dto.Image;
import pl.marwit44.orderNowbackend.model.ImageModel;
import pl.marwit44.orderNowbackend.model.ItemDetailsModel;
import pl.marwit44.orderNowbackend.repository.ImageRepository;
import pl.marwit44.orderNowbackend.repository.ItemDetailsRepository;
import pl.marwit44.orderNowbackend.transformer.ImageTransformer;
import pl.marwit44.orderNowbackend.transformer.ItemDetailsTransformer;

import java.time.LocalDateTime;
import java.util.List;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ImageServiceTest {

    private ImageService imageService;

    @Autowired
    private ImageRepository imageRepository;

    @Autowired
    private ItemDetailsRepository itemDetailsRepository;

    @Before
    public void init() {

        imageService = new ImageService(imageRepository);

        ItemDetailsModel itemDetails = ItemDetailsModel.Builder.anItemDetailsModel()
                .withId(1L)
                .withDescription("size 50x100 cm")
                .withAddDate(LocalDateTime.now())
                .build();

        ImageModel image = ImageModel.Builder.anImageModel()
                .withId(1L)
                .withName("book.png")
                .withType("image/png")
                .withItemDetailsModel(itemDetails)
                .withAddDate(LocalDateTime.now())
                .build();

        itemDetailsRepository.save(itemDetails);
    }

    @Test
    public void shouldSaveImage() {

        // Given
        List<ItemDetailsModel> itemDetailsList = itemDetailsRepository.findAll();
        Image image = Image.Builder.anImage()
                .withName("car.png")
                .withType("image/png")
                .withAddDate(LocalDateTime.now())
                .withItemDetails(ItemDetailsTransformer.getItemDetailsDtoFromModel(itemDetailsList.get(0)))
                .build();

        // When
        imageService.saveImage(image);

        List<ImageModel> imageList = imageRepository.findAll();

        // Then
        Assertions.assertThat(imageList.stream().anyMatch(e -> e.getName().equals("car.png"))).isTrue();
    }

    @Test
    public void shouldDeleteImage() {

        // Given
        List<ItemDetailsModel> itemDetailsList = itemDetailsRepository.findAll();
        Image image = Image.Builder.anImage()
                .withName("book.png")
                .withType("image/png")
                .withAddDate(LocalDateTime.now())
                .withItemDetails(ItemDetailsTransformer.getItemDetailsDtoFromModel(itemDetailsList.get(0)))
                .build();

        imageService.saveImage(image);

        // When
        imageService.deleteImage(ImageTransformer.getImageDtoFromModel(imageRepository.findAll().get(0)));
        List<ImageModel> imageList = imageRepository.findAll();

        // Then
        Assertions.assertThat(imageList.size() == 0).isTrue();
    }
}
