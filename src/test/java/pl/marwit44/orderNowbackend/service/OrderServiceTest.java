package pl.marwit44.orderNowbackend.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.marwit44.orderNowbackend.dto.Order;
import pl.marwit44.orderNowbackend.exceptions.OrderNotFoundException;
import pl.marwit44.orderNowbackend.model.OrderModel;
import pl.marwit44.orderNowbackend.model.OrderStatus;
import pl.marwit44.orderNowbackend.model.UserRole;
import pl.marwit44.orderNowbackend.repository.OrderRepository;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderServiceTest {

    private OrderService orderService;

    @Autowired
    private OrderRepository orderRepository;

    @Before
    public void init() {

        orderService = new OrderService(orderRepository);

        OrderModel order = OrderModel.Builder.anOrderModel()
                .withPrice(99.9)
                .withAccepted(true)
                .withUserId(1L)
                .withStatus(OrderStatus.PREPARATION)
                .withAddDate(LocalDateTime.now())
                .build();

        OrderModel order2 = OrderModel.Builder.anOrderModel()
                .withPrice(29.92)
                .withAccepted(true)
                .withUserId(1L)
                .withStatus(OrderStatus.SENDING)
                .withAddDate(LocalDateTime.now())
                .build();

        OrderModel order3 = OrderModel.Builder.anOrderModel()
                .withPrice(199.9)
                .withAccepted(true)
                .withUserId(2L)
                .withStatus(OrderStatus.DONE)
                .withAddDate(LocalDateTime.now())
                .build();

        OrderModel order4 = OrderModel.Builder.anOrderModel()
                .withPrice(19.9)
                .withAccepted(false)
                .withUserId(2L)
                .withStatus(OrderStatus.PREPARATION)
                .withAddDate(LocalDateTime.now())
                .build();

        orderRepository.saveAll(Arrays.asList(order, order2, order3, order4));
    }

    @Test
    public void shouldGetAllOrdersWithPagination() {

        //Given

        //When
        List<Order> orderListForUser = orderService.getAllOrdersWithPaginationAndFilter(1L, UserRole.ROLE_USER, 1, null, "addDate", false, -1, -1, LocalDateTime.now(), LocalDateTime.now());
        List<Order> orderListForAdmin = orderService.getAllOrdersWithPaginationAndFilter(1L, UserRole.ROLE_ADMIN, 1, null, "price", true, -1, -1, LocalDateTime.now(), LocalDateTime.now());

        //Then
        Assertions.assertThat(orderListForUser.size()).isEqualTo(2);
        Assertions.assertThat(orderListForAdmin.size()).isEqualTo(3);
        Assertions.assertThat(orderListForAdmin.get(0).getPrice() == 199.9);
        Assertions.assertThat(orderListForAdmin.get(0).getStatus() == OrderStatus.DONE);
    }

    @Test
    public void shouldGetOrdersWithPaginationAndFilter() {

        // Given

        // When
        List<Order> orderListForUser = orderService.getAllOrdersWithPaginationAndFilter(1L, UserRole.ROLE_USER, 1, "d", "addDate", false, -1, -1, LocalDateTime.now(), LocalDateTime.now());
        List<Order> orderListForAdmin = orderService.getAllOrdersWithPaginationAndFilter(1L, UserRole.ROLE_ADMIN, 1, "d", "addDate", false, -1, -1, LocalDateTime.now(), LocalDateTime.now());
        List<Order> orderListForAdmin2 = orderService.getAllOrdersWithPaginationAndFilter(1L, UserRole.ROLE_ADMIN, 1, "d", "price", false, -1, 30, LocalDateTime.now(), LocalDateTime.now());

        // Then
        Assertions.assertThat(orderListForUser.size()).isEqualTo(1);
        Assertions.assertThat(orderListForUser.stream().allMatch(e -> e.getStatus().toString().contains("D"))).isTrue();
        Assertions.assertThat(orderListForAdmin.size()).isEqualTo(2);
        Assertions.assertThat(orderListForAdmin.stream().allMatch(e -> e.getStatus().toString().contains("D"))).isTrue();
        Assertions.assertThat(orderListForAdmin2.size()).isEqualTo(1);
    }

    @Test
    public void shouldGetNotAcceptedOrder() {

        // Given

        // When
        Order order = orderService.getNotAccepted(2L);

        // Then
        Assertions.assertThat(order.getPrice() == 19.9);
    }

    @Test
    public void shouldGetOrderById() throws OrderNotFoundException {

        // Given
        List<Order> orderListForAdmin = orderService.getAllOrdersWithPaginationAndFilter(1L, UserRole.ROLE_ADMIN, 1, null, "addDate", false, -1, -1, LocalDateTime.now(), LocalDateTime.now());
        Order orderForTest = orderListForAdmin.get(0);

        // When
        Order returnedOrder = orderService.getById(orderForTest.getId());

        // Then
        Assertions.assertThat(returnedOrder.getPrice() == orderForTest.getPrice());
        Assertions.assertThat(returnedOrder.getUserId().equals(orderForTest.getUserId()));
    }

    @Test
    public void shouldNotGetOrderById() {

        // Given

        // When
        Throwable throwable = catchThrowable(() -> orderService.getById(11111L));

        // Then
        assertThat(throwable).isInstanceOf(OrderNotFoundException.class);
    }

    @Test
    public void shouldDeleteOrder() {

        // Given
        List<Order> orderListForAdmin = orderService.getAllOrdersWithPaginationAndFilter(1L, UserRole.ROLE_ADMIN, 1, null, "addDate", false, -1, -1, LocalDateTime.now(), LocalDateTime.now());
        Order orderForTest = orderListForAdmin.get(0);

        // When
        orderService.deleteOrder(orderForTest);
        Throwable throwable = catchThrowable(() -> orderService.getById(orderForTest.getId()));

        // Then
        assertThat(throwable).isInstanceOf(OrderNotFoundException.class);
    }

    @Test
    public void shouldEditOrder() {

        // Given
        List<Order> orderListForAdmin = orderService.getAllOrdersWithPaginationAndFilter(1L, UserRole.ROLE_ADMIN, 1, null, "addDate", false, -1, -1, LocalDateTime.now(), LocalDateTime.now());
        Order orderForTest = orderListForAdmin.get(0);

        orderForTest.setStatus(OrderStatus.READY_TO_DELIVER);
        orderForTest.setPrice(0.1);

        // When
        orderService.editOrder(orderForTest);

        // Then
        Assertions.assertThat(orderForTest.getPrice() == 0.1);
        Assertions.assertThat(orderForTest.getStatus() == OrderStatus.READY_TO_DELIVER);
    }

    @Test
    public void shouldAcceptOrder() {

        // Given
        Order orderForTest = orderService.getNotAccepted(2L);

        // When
        orderService.acceptOrder(orderForTest);

        // Then
        Assertions.assertThat(orderForTest.isAccepted());
    }
}