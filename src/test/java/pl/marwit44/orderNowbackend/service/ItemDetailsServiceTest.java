package pl.marwit44.orderNowbackend.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.marwit44.orderNowbackend.dto.ItemDetails;
import pl.marwit44.orderNowbackend.exceptions.ItemDetailsNotFoundException;
import pl.marwit44.orderNowbackend.model.ItemDetailsModel;
import pl.marwit44.orderNowbackend.model.ItemModel;
import pl.marwit44.orderNowbackend.repository.ItemDetailsRepository;
import pl.marwit44.orderNowbackend.repository.ItemRepository;
import pl.marwit44.orderNowbackend.transformer.ItemDetailsTransformer;

import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ItemDetailsServiceTest {

    private ItemDetailsService itemDetailsService;

    @Autowired
    private ItemDetailsRepository itemDetailsRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Before
    public void init() {

        itemDetailsService = new ItemDetailsService(itemDetailsRepository);

        ItemModel item = ItemModel.Builder.anItemModel()
                .withName("blackboard")
                .withPrice(149.56)
                .withAmount(2)
                .withAddDate(LocalDateTime.now())
                .build();

        ItemDetailsModel itemDetails = ItemDetailsModel.Builder.anItemDetailsModel()
                .withId(1L)
                .withDescription("size 50x100 cm")
                .withAddDate(LocalDateTime.now())
                .build();

        itemDetailsRepository.save(itemDetails);

        item.setItemDetailsModel(itemDetails);
        itemRepository.save(item);
    }

    @Test
    public void shouldGetItemDetailsById() throws ItemDetailsNotFoundException {

        // Given
        List<ItemModel> itemList = itemRepository.findAll();
        ItemModel itemForTest = itemList.get(0);
        ItemDetailsModel itemDetailsForTest = itemForTest.getItemDetailsModel();

        // When
        ItemDetails itemDetailsReturned = itemDetailsService.getById(itemDetailsForTest.getId());

        // Then
        Assertions.assertThat(itemDetailsReturned.getAddDate() == itemDetailsForTest.getAddDate());
        Assertions.assertThat(itemDetailsReturned.getDescription().equals(itemDetailsForTest.getDescription()));
    }

    @Test
    public void shouldNotGetItemInOrderById() {

        Throwable throwable = catchThrowable(() -> itemDetailsService.getById(-1111L));

        // Then
        assertThat(throwable).isInstanceOf(ItemDetailsNotFoundException.class);
    }

    @Test
    public void shouldEItemDetailsById() throws ItemDetailsNotFoundException {

        // Given
        List<ItemModel> itemList = itemRepository.findAll();
        ItemModel itemForTest = itemList.get(0);
        ItemDetailsModel itemDetailsForTest = itemForTest.getItemDetailsModel();

        // When
        itemDetailsForTest.setDescription("aaa");
        itemDetailsService.editDetails(ItemDetailsTransformer.getItemDetailsDtoFromModel(itemDetailsForTest));
        ItemDetails itemDetailsReturned = itemDetailsService.getById(itemDetailsForTest.getId());

        // Then
        Assertions.assertThat(itemDetailsReturned.getDescription().equals("aaa"));
        Assertions.assertThat(itemDetailsReturned.getDescription().equals(itemDetailsForTest.getDescription()));
    }
}
