package pl.marwit44.orderNowbackend.service;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.marwit44.orderNowbackend.dto.ItemInOrder;
import pl.marwit44.orderNowbackend.exceptions.ItemNotFoundException;
import pl.marwit44.orderNowbackend.model.ItemInOrderModel;
import pl.marwit44.orderNowbackend.model.OrderModel;
import pl.marwit44.orderNowbackend.model.OrderStatus;
import pl.marwit44.orderNowbackend.repository.ItemInOrderRepository;
import pl.marwit44.orderNowbackend.repository.OrderRepository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.catchThrowable;

@RunWith(SpringRunner.class)
@DataJpaTest
public class ItemInOrderServiceTest {

    private ItemInOrderService itemInOrderService;

    @Autowired
    private ItemInOrderRepository itemInOrderRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Before
    public void init() {

        itemInOrderService = new ItemInOrderService(itemInOrderRepository, orderRepository);

        OrderModel order = OrderModel.Builder.anOrderModel()
                .withPrice(99.9)
                .withAccepted(true)
                .withUserId(1L)
                .withStatus(OrderStatus.PREPARATION)
                .withAddDate(LocalDateTime.now())
                .build();

        OrderModel order2 = OrderModel.Builder.anOrderModel()
                .withPrice(29.92)
                .withAccepted(true)
                .withUserId(1L)
                .withStatus(OrderStatus.SENDING)
                .withAddDate(LocalDateTime.now())
                .build();

        OrderModel order3 = OrderModel.Builder.anOrderModel()
                .withPrice(99.9)
                .withAccepted(true)
                .withUserId(1L)
                .withStatus(OrderStatus.DONE)
                .withAddDate(LocalDateTime.now())
                .build();

        ItemInOrderModel itemInOrder = ItemInOrderModel.Builder.anItemInOrderModel()
                .withName("chalk")
                .withPrice(4.99)
                .withAmount(5)
                .withOrder(order)
                .withAddDate(LocalDateTime.now())
                .build();

        ItemInOrderModel itemInOrder2 = ItemInOrderModel.Builder.anItemInOrderModel()
                .withName("blackboard")
                .withPrice(149.56)
                .withAmount(2)
                .withOrder(order)
                .withAddDate(LocalDateTime.now())
                .build();

        ItemInOrderModel itemInOrder3 = ItemInOrderModel.Builder.anItemInOrderModel()
                .withName("board")
                .withPrice(49.56)
                .withAmount(3)
                .withOrder(order3)
                .withAddDate(LocalDateTime.now())
                .build();


        order.setItemList(Arrays.asList(itemInOrder, itemInOrder2));
        order2.setItemList(new ArrayList<>());
        order3.setItemList(Collections.singletonList(itemInOrder3));

        orderRepository.save(order);
        orderRepository.save(order2);
        orderRepository.save(order3);
    }

    @Test
    public void shouldGetItemsInOrderWithPagination() {

        //Given
        List<OrderModel> orderList = orderRepository.findAll();
        OrderModel orderForTest = orderList.get(0);

        //When
        List<ItemInOrder> itemInOrderList = itemInOrderService.getItemsInOrderListWithPaginationAndFilter(orderForTest.getId(), 1, null, "amount", true, 1, 5);

        //Then
        Assertions.assertThat(itemInOrderList.size()).isEqualTo(2);
        Assertions.assertThat(itemInOrderList.get(0).getName().equals("chalk"));
    }

    @Test
    public void shouldGetItemsInOrderWithPaginationAndFilter() {

        //Given
        List<OrderModel> orderList = orderRepository.findAll();
        OrderModel orderForTest = orderList.get(0);

        //When
        List<ItemInOrder> itemInOrderList = itemInOrderService.getItemsInOrderListWithPaginationAndFilter(orderForTest.getId(), 1, "ch", "name", false, -1, -1);
        List<ItemInOrder> itemInOrderList2 = itemInOrderService.getItemsInOrderListWithPaginationAndFilter(orderForTest.getId(), 1, "ch", "price", false, 0, 2);

        //Then
        Assertions.assertThat(itemInOrderList.size()).isEqualTo(1);
        Assertions.assertThat(itemInOrderList.stream().allMatch(e -> e.getName().contains("ch"))).isTrue();
        Assertions.assertThat(itemInOrderList2.size()).isEqualTo(0);
    }

    @Test
    public void shouldGetItemInOrderById() throws ItemNotFoundException {

        // Given
        List<OrderModel> orderList = orderRepository.findAll();
        OrderModel orderForTest = orderList.get(0);
        ItemInOrderModel itemInOrderForTest = orderForTest.getItemList().get(0);

        // When
        ItemInOrder itemInOrderReturned = itemInOrderService.getById(itemInOrderForTest.getId());

        // Then
        Assertions.assertThat(itemInOrderReturned.getPrice() == itemInOrderForTest.getPrice());
        Assertions.assertThat(itemInOrderReturned.getName().equals(itemInOrderForTest.getName()));
    }

    @Test
    public void shouldNotGetItemInOrderById() {

        // Given

        // When
        Throwable throwable = catchThrowable(() -> itemInOrderService.getById(11111L));

        // Then
        assertThat(throwable).isInstanceOf(ItemNotFoundException.class);
    }

    @Test
    public void shouldDeleteItemFromOrder() throws ItemNotFoundException {

        // Given
        List<OrderModel> orderList = orderRepository.findAll();
        OrderModel orderForTest = orderList.get(0);
        ItemInOrder itemInOrderForTest = itemInOrderService.getById(orderForTest.getItemList().get(0).getId());

        // When
        itemInOrderService.deleteItemFromOrder(itemInOrderForTest, orderForTest.getId());
        Throwable throwable = catchThrowable(() -> itemInOrderService.getById(itemInOrderForTest.getId()));

        // Then
        assertThat(throwable).isInstanceOf(ItemNotFoundException.class);
    }

    @Test
    public void shouldDeleteItemsFromOrderAndOrder() throws ItemNotFoundException {

        // Given
        List<OrderModel> orderList = orderRepository.findAll();
        OrderModel orderForTest = orderList.get(0);
        ItemInOrder itemInOrderForTest = itemInOrderService.getById(orderForTest.getItemList().get(0).getId());
        ItemInOrder itemInOrderForTest2 = itemInOrderService.getById(orderForTest.getItemList().get(1).getId());

        // When
        itemInOrderService.deleteItemFromOrder(itemInOrderForTest, orderForTest.getId());
        itemInOrderService.deleteItemFromOrder(itemInOrderForTest2, orderForTest.getId());

        Throwable throwable = catchThrowable(() -> itemInOrderService.getById(itemInOrderForTest.getId()));
        Throwable throwable2 = catchThrowable(() -> itemInOrderService.getById(itemInOrderForTest2.getId()));

        orderList = orderRepository.findAll();
        OrderModel orderForTest2 = orderList.get(1);
        ItemInOrder itemInOrderForTest3 = itemInOrderService.getById(orderForTest2.getItemList().get(0).getId());

        itemInOrderForTest3.setAmount(1);
        itemInOrderService.deleteItemFromOrder(itemInOrderForTest, orderForTest.getId());

        orderList = orderRepository.findAll();
        OrderModel orderReturned = orderList.get(1);
        ItemInOrder itemInOrderReturned = itemInOrderService.getById(orderForTest2.getItemList().get(0).getId());

        // Then
        assertThat(throwable).isInstanceOf(ItemNotFoundException.class);
        assertThat(throwable2).isInstanceOf(ItemNotFoundException.class);

        Assertions.assertThat(orderList.size()).isEqualTo(2);

        Assertions.assertThat(orderReturned.getItemList().size()).isEqualTo(1);
        Assertions.assertThat(itemInOrderReturned.getName().equals("board"));
        Assertions.assertThat(itemInOrderReturned.getAmount() == 2);
    }

    @Test
    public void shouldCheckAllWitchNamesAndReturnTrue()
    {
        // Given

        // When
        boolean result = itemInOrderService.checkAllWithName("blackboard");

        // Then
        Assertions.assertThat(result);
    }

    @Test
    public void shouldCheckAllWitchNamesAndReturnFalse()
    {
        // Given

        // When
        boolean result = itemInOrderService.checkAllWithName("board");

        // Then
        Assertions.assertThat(result);
    }
}